package com.emids.healthInsurance.pojo;

public class PolicyOwner {
	public String name;
	public String gender;
	public int age;
	public CurrentHealth currentHealth;
	public Habbits habbits;
	public int premimumAmount;
	
	public int getPremimumAmount() {
		return premimumAmount;
	}
	public void setPremimumAmount(int premimumAmount) {
		this.premimumAmount = premimumAmount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Habbits getHabbits() {
		return habbits;
	}
	public void setHabbits(Habbits habbits) {
		this.habbits = habbits;
	}
	
	
	
	

}
