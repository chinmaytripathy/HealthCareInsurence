package com.emids.healthInsurance.pojo;

public class Habbits {
	public boolean smoking;
	public boolean alcohol;
	public  boolean dailyExcerise;
	public boolean drugs;

	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohol() {
		return alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}
	public boolean isDailyExcerise() {
		return dailyExcerise;
	}
	public void setDailyExcerise(boolean dailyExcerise) {
		this.dailyExcerise = dailyExcerise;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	
}
