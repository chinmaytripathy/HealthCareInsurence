/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emids.healthInsurance.service;

import com.emids.healthInsurance.pojo.PolicyOwner;

/**
 *
 * @author CHINMAY
 */
public interface PolicyPremiumCalculation {

    public PolicyOwner getPremiumAmount(PolicyOwner policyOwner, int baseAmount);

}
