/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emids.healthInsurance.util;

import com.emids.healthInsurance.pojo.PolicyOwner;

/**
 *
 * @author CHINMAY
 */
public class PolicyUtil {

    public static int getAmountByGender(PolicyOwner policyOwner, int basePremiumValue) {
        int nGenderAmountMoney = 0;
        if (policyOwner.getGender().equalsIgnoreCase("MALE")) {
            nGenderAmountMoney = (int) (basePremiumValue * (PolicyConstants.genderPFercentage / 100.0f));
        }
        basePremiumValue = basePremiumValue + nGenderAmountMoney;
        return basePremiumValue;
    }

    public static int getAmountByHealthStatus(PolicyOwner policyOwner, int baseAmount) {
        int nHealthAmountMoney = 0;
        if (policyOwner.getCurrentHealth().isBloodPressure() || policyOwner.getCurrentHealth().isBloodSugar() || policyOwner.getCurrentHealth().isHypertension() || policyOwner.getCurrentHealth().isOverweight()) {
            nHealthAmountMoney = (int) (baseAmount * (PolicyConstants.currentHealthCondition / 100.0f));
        }
        baseAmount = baseAmount + nHealthAmountMoney;
        return baseAmount;
    }

    public static  int getAmountByDailyExcersise(PolicyOwner policyOwner, int baseAmount) {
        int nAmountDailyExcersiseMoney = 0;
        nAmountDailyExcersiseMoney = (int) (baseAmount * (PolicyConstants.dailyExcersise / 100.0f));
        baseAmount = baseAmount - nAmountDailyExcersiseMoney;
        return baseAmount;
    }

    public static  int getAmountByHabbits(PolicyOwner policyOwner, int baseAmount) {
        int nAmountHabbits = 0;
        nAmountHabbits = (int) (baseAmount * (PolicyConstants.habbits / 100.0f));
        baseAmount = baseAmount + nAmountHabbits;
        return baseAmount;
    }

    public  static  int calculateAgeLessThan18() {
        return PolicyConstants.basePremiumAmount;
    }

    public static  int calculateAgeBetween18To25(int baseAmount, PolicyOwner policyOwner) {
        int temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        baseAmount = getAmountByGender(policyOwner, baseAmount);
        baseAmount = getAmountByHealthStatus(policyOwner, baseAmount);
        baseAmount = getAmountByDailyExcersise(policyOwner, baseAmount);
        baseAmount = getAmountByHabbits(policyOwner, baseAmount);

        return baseAmount;
    }

    public static  int calculateAgeBetween25To30(int baseAmount, PolicyOwner policyOwner) {
        int temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        baseAmount = getAmountByGender(policyOwner, baseAmount);
        baseAmount = getAmountByHealthStatus(policyOwner, baseAmount);
        baseAmount = getAmountByDailyExcersise(policyOwner, baseAmount);
        baseAmount = getAmountByHabbits(policyOwner, baseAmount);

        return baseAmount;
    }

    public static  int calculateAgeBetween30To35(int baseAmount, PolicyOwner policyOwner) {
        int temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        baseAmount = getAmountByGender(policyOwner, baseAmount);
        baseAmount = getAmountByHealthStatus(policyOwner, baseAmount);
        baseAmount = getAmountByDailyExcersise(policyOwner, baseAmount);
        baseAmount = getAmountByHabbits(policyOwner, baseAmount);

        return baseAmount;
    }

    public static  int calculateAgeBetween35To40(int baseAmount, PolicyOwner policyOwner) {
        int temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        baseAmount = getAmountByGender(policyOwner, baseAmount);
        baseAmount = getAmountByHealthStatus(policyOwner, baseAmount);
        baseAmount = getAmountByDailyExcersise(policyOwner, baseAmount);
        baseAmount = getAmountByHabbits(policyOwner, baseAmount);

        return baseAmount;
    }

    public  static  int calculateAgeGreaterThan40(int baseAmount, PolicyOwner policyOwner) {
        int temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentage / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        temp = (int) (baseAmount * (PolicyConstants.incresePercentageMoreThan40Age / PolicyConstants.divideBy));
        baseAmount = baseAmount + temp;
        baseAmount = getAmountByGender(policyOwner, baseAmount);
        baseAmount = getAmountByHealthStatus(policyOwner, baseAmount);
        baseAmount = getAmountByDailyExcersise(policyOwner, baseAmount);

        return baseAmount;
    }
}
