package com.emids.healthInsurance.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.emids.healthInsurance.pojo.CurrentHealth;
import com.emids.healthInsurance.pojo.Habbits;
import com.emids.healthInsurance.pojo.PolicyOwner;
import com.emids.healthInsurance.service.PolicyPremiumCalculation;
import com.emids.healthInsurance.serviceImpl.PolicyPremiumCalculationImpl;
import com.emids.healthInsurance.util.PolicyConstants;

public class HealthInsuranceMain {

    public static void main(String[] str) {
        try {
            System.out.println("********WELCOME TO HEALTH INSURANCE APPLICTION*************");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Please enter person Name:");
            String strName = br.readLine();
            System.out.println("Please enter person Age:");
            String age = br.readLine();
            int nAge = Integer.parseInt(age);

            System.out.println("Please enter Gender :");
            String strGender = br.readLine();

            System.out.println("Please enter Current Health Status:");
            System.out.println("Is person is Hypertension: ?");
            String isStrHypertension = br.readLine();
            boolean isisHypertension = isStrHypertension.equalsIgnoreCase("Y") ? true : false;

            System.out.println("Is person is Blood Pressure: ?");
            String isStrBloodPressure = br.readLine();
            boolean isBloodPressure = isStrBloodPressure.equalsIgnoreCase("Y") ? true : false;

            System.out.println("Is person is Blood Sugar: ?");
            String isStrBloodSugar = br.readLine();
            boolean isBloodSugar = isStrBloodSugar.equalsIgnoreCase("Y") ? true : false;

            System.out.println("Is person is Over weight: ?");
            String isStrOverweight = br.readLine();
            boolean isOverweight = isStrOverweight.equalsIgnoreCase("Y") ? true : false;

            System.out.println("Please enter  Person Habbits:");
            System.out.println("Is person is Smoking: ?");
            String isStrSmooking = br.readLine();
            boolean isSmooking = isStrSmooking.equalsIgnoreCase("Y") ? true : false;

            System.out.println("Is person is Alchol: ?");
            String isStrAlchol = br.readLine();
            boolean isAlchol = isStrAlchol.equalsIgnoreCase("Y") ? true : false;

            System.out.println("Is person is Daily Excersise: ?");
            String isStrExcersise = br.readLine();
            boolean isExcersise = isStrExcersise.equalsIgnoreCase("Y") ? true : false;

            System.out.println("Is person is Drugs: ?");
            String isStrDrugs = br.readLine();
            boolean isDrugs = isStrDrugs.equalsIgnoreCase("Y") ? true : false;

            PolicyOwner policyOwner = new PolicyOwner();
            policyOwner.setName(strName);
            policyOwner.setAge(nAge);
            policyOwner.setGender(strGender);
            CurrentHealth currentHealth = new CurrentHealth();
            currentHealth.setBloodPressure(isBloodPressure);
            currentHealth.setBloodSugar(isBloodSugar);
            currentHealth.setHypertension(isisHypertension);
            currentHealth.setOverweight(isOverweight);

            policyOwner.setCurrentHealth(currentHealth);

            Habbits habbits = new Habbits();
            habbits.setAlcohol(isAlchol);
            habbits.setDailyExcerise(isExcersise);
            habbits.setDrugs(isDrugs);
            habbits.setSmoking(isSmooking);

            policyOwner.setHabbits(habbits);


            PolicyPremiumCalculation premiumCalculation = new PolicyPremiumCalculationImpl();
            policyOwner = premiumCalculation.getPremiumAmount(policyOwner, PolicyConstants.basePremiumAmount);
            System.out.println("Person Name : " + policyOwner.getName() + "is: " + policyOwner.getPremimumAmount());
            
            
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

    }

}
