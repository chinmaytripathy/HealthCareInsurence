/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emids.healthInsurance.serviceImpl;

import com.emids.healthInsurance.pojo.PolicyOwner;
import com.emids.healthInsurance.service.PolicyPremiumCalculation;
import com.emids.healthInsurance.util.PolicyUtil;

/**
 *
 * @author CHINMAY
 */
public class PolicyPremiumCalculationImpl implements PolicyPremiumCalculation {

    public PolicyOwner getPremiumAmount(PolicyOwner policyOwner, int basePremimum) {
        int premimumMoney = 0;
        try {
            if (policyOwner != null) {
                if (policyOwner.getAge() < 18) {
                    premimumMoney = PolicyUtil.calculateAgeLessThan18();

                } else if (policyOwner.getAge() > 18 && policyOwner.getAge() < 25) {
                    premimumMoney = PolicyUtil.calculateAgeBetween18To25(basePremimum, policyOwner);

                } else if (policyOwner.getAge() > 25 && policyOwner.getAge() < 30) {
                    premimumMoney = PolicyUtil.calculateAgeBetween30To35(basePremimum, policyOwner);

                } else if (policyOwner.getAge() > 30 && policyOwner.getAge() < 35) {
                    premimumMoney = PolicyUtil.calculateAgeBetween30To35(basePremimum, policyOwner);
                } else if (policyOwner.getAge() > 35 && policyOwner.getAge() < 40) {
                    premimumMoney = PolicyUtil.calculateAgeBetween35To40(basePremimum, policyOwner);
                } else if (policyOwner.getAge() > 40) {
                    premimumMoney = PolicyUtil.calculateAgeGreaterThan40(basePremimum, policyOwner);
                }
            }
            policyOwner.setPremimumAmount(premimumMoney);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return policyOwner;
    }

}
