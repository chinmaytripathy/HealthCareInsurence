/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emids.healthInsurance.util;

/**
 *
 * @author CHINMAY
 */
public class PolicyConstants {

    public static final int basePremiumAmount = 5000;//Base amount can load from properties file/configuration
    public static final float genderPFercentage = 2.0f;
    public static final float currentHealthCondition = 1.0f;
    public static final float dailyExcersise = 3.0f;
    public static final float habbits = 3.0f;
    public static final float incresePercentage = 10.0f;
    public static final float incresePercentageMoreThan40Age = 20.0f;
    public static final float divideBy = 100.0f;
    public static final String genderMale = "MALE";
    public static final String genderFeMale = "FEMALE";
}
